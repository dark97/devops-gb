nexus:
  repos:
    proxy:
      cassandra:
        repo_type: apt
        remote_url: http://www.apache.org/dist/cassandra/debian
        distribution: 40x
        blob_store: deb
        content_max_age: 1440
        metadata_max_age: 10
        policyNames:
          'deb_cleanup'
        api_url: 'v1/repositories'

  nginx:
    site:
      cassandra.repo.russianpost.ru:
        name: cassandra
        enabled: true
        ssl:
          enabled: true
        host:
          name: cassandra.repo.russianpost.ru
        rewrite:
          - ^/(.*) /deb/repository/cassandra/$1
        repo_type: apt
